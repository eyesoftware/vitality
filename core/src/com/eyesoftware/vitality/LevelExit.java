package com.eyesoftware.vitality;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.SerializationException;

/**
 * An entity that will cause the elvel to change when collided with. It's an entity because it allows the use of the establed collision detection.
 * @Author Bobby Ross
 */
public class LevelExit extends Entity {
    private String levelToGoTo;

    private Vitality game;

    /**
     * Constructs a new Level Exit
     * NOTE: There is no Id, all exits have Id of 999
     * @param x X-coordinate
     * @param y Y-coordinate
     * @param sprite sprite
     * @param game game
     */
    public LevelExit(int x, int y, Sprite sprite,  Vitality game, String levelToGoTo){
        super(999,x,y,sprite);
        this.game = game;
        this.levelToGoTo = levelToGoTo;
    }

    /**
     * Checks if the entity collided with is a player. Then changes the level if it is
     * @param e Entity collided with
     */
    @Override
    public void Collideswith(Entity e) {
        System.out.println("Exit Collision with" + e);
        try{if(e.getId() == 0){
            System.out.println("Loading: " + levelToGoTo);
            game.switchLevel(levelToGoTo);
            game.create();

        }}catch (SerializationException f){
            System.out.println("Could not load map file: " + levelToGoTo);
        }
        return;
    }

    /**
     * Does not move
     * @param x
     * @param y
     * @param level
     * @return
     */
    @Override
    public boolean move(int x, int y, Level level){
        return false;
    }

    /**
     * Does not allow movement
     * @param x        X-Coordinate
     * @param y        Y-COordinate
     * @param level    Level to traverse
     */
    public void moveTo(int x, int y, Level level){
        return;
    }

    /**
     * Does not render
     * @param batch
     */
    public void render(SpriteBatch batch){
        return;
    }
}
