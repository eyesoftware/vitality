package com.eyesoftware.vitality;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
/**
 * Defines the deafualt behavior for an Entity Object.
 * @Author Bobby
 *
 */
public class Player extends Entity {
    private Vitality game;

    public Player(int id, int x, int y, Sprite sprite, Vitality game) {
        super(id, x, y, sprite);
        this.game = game;
    }

    @Override
    public void Collideswith(Entity e) {
        System.out.println("Collision with: " + e.toString());
        /* TODO: Create a battle scene with given entity */
    }


    @Override
    public boolean move(int x, int y, Level level){
        return move(x,y,level,1);
    }
    public boolean move(int x, int y, Level level, int delta){

        if (y > 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[3][0]);
            for(Entity e :level.getEntities()){
                e.calculateMove(this,level,delta);
            }
            return super.move(x, y, level);

        } else if (y < 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[0][0]);
            for(Entity e :level.getEntities()){

                e.calculateMove(this,level,delta);

            }
            return super.move(x, y, level);

        }
        if (x > 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[2][0]);
            for(Entity e :level.getEntities()){

                e.calculateMove(this,level,delta);

            }
            return super.move(x, y, level);

        } else if (x < 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[1][0]);
            for(Entity e :level.getEntities()){

                e.calculateMove(this,level,delta);

            }
            return super.move(x, y, level);

        }

        return false;

    }
    @Override
    public void render(SpriteBatch batch){
        Rectangle scissor = new Rectangle();
        Rectangle clipBounds = new Rectangle(0, 0, Tile.TILE_SIZE, Tile.TILE_SIZE);
        ScissorStack.calculateScissors(game.camera, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), batch.getTransformMatrix(), clipBounds, scissor);
        super.render(batch);
        ScissorStack.pushScissors(scissor);
        ScissorStack.popScissors();

    }


    public static Player constructPlayer(int[] spawn, Vitality game) {
        System.out.print(spawn[1]);
        Player player = new Player(0, spawn[0] * Tile.TILE_SIZE, spawn[1] * Tile.TILE_SIZE, new Sprite(new TextureRegion(new Texture(Gdx.files.internal("player.png")), Tile.TILE_SIZE, Tile.TILE_SIZE)),game);
        Gdx.app.log("Info", "Player Spawned at:" + "(" + spawn[0] + "," + spawn[1] + ")");
            player.setRegionOfTexture(new TextureRegion(new Texture(Gdx.files.internal("player.png"))).split(Tile.TILE_SIZE, Tile.TILE_SIZE));

        return player;
    }
}

