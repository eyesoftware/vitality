package com.eyesoftware.vitality;

/**
 * Created by Bobby on 12/12/2014.
 */
public class PlayerSpawnNotFound extends Exception {
    public PlayerSpawnNotFound(String message) {
        super(message);
    }
}
