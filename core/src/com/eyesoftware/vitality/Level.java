package com.eyesoftware.vitality;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bobby on 12/11/2014.
 */
public class Level {
    private boolean[][] collision = null;
    private TiledMap levelMap = null;
    List<Entity> entities = new ArrayList<Entity>();
    List<Item> items = new ArrayList<Item>();
    private String id;

    /**
     * Construct a level
     * @param map
     * @param id
     * @param collision
     */
    public Level(TiledMap map, String id, boolean[][] collision,List<Entity> entities) {
        this.levelMap = map;
        this.collision = collision;
        this.id = id;
        this.entities = new ArrayList<Entity>(entities);
    }

    public TiledMap getLevelMap() {
        return levelMap;
    }

    /**
     * Gets the player spawn, if there are stupidly two spawns, it gets the first one in the map.
     *
     * @return Array containing the spawn point in Grid Coordinates (Must be multiplied by tile size)
     */
    public int[] getPlayerSpawn() throws PlayerSpawnNotFound {
        TiledMapTileLayer layer = (TiledMapTileLayer) levelMap.getLayers().get(3);
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                try {
                    TiledMapTile tile = cell.getTile();
                    if (tile.getProperties().containsKey("spawn")) {
                        int[] returnedArray = {x, y};
                        return returnedArray;
                    }
                } catch (NullPointerException e) {
                    continue;
                }
            }
        }
        throw new PlayerSpawnNotFound("Player Spawn Not Found; Cannot find Spawn tile in map: " + id + "; Unable to create Player object" + " \n Attention Modders:\n" +
                "\tPlease consult documentation on Map design\n"
        );
    }

    public boolean checkCollision(int x, int y) {
        TiledMapTileLayer layer = (TiledMapTileLayer) levelMap.getLayers().get(1);

        try {
            if (layer.getCell(x, y).getTile() != null) {
                return true;

            }
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }

    public List<Entity> getEntities(){
        return entities;
    }

    public void render(SpriteBatch batch) {

        for (Entity e : entities) {
            e.render(batch);
        }
        for (Item i : items) {
            i.render(batch);
        }
    }


}
