package com.eyesoftware.vitality;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Most basic enemy, Follows Player
 */
public class Enemy extends Entity {
    private int numberOnSpriteSheetY;
    @Override
    public void Collideswith(Entity e) {

    }
    public Enemy/*#1*/(int id, int x, int y, Sprite sprite){
        super(id,x,y,sprite);
    }
    public Enemy(int numberOnSpriteSheetY, int id, int x, int y, Sprite sprite){
        super(id,x,y,sprite);
        this.numberOnSpriteSheetY = numberOnSpriteSheetY;
    }

    @Override
    public boolean move(int x, int y, Level level) {
        if (y > 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[numberOnSpriteSheetY][7]);
            return super.move(x, y, level);

        } else if (y < 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[numberOnSpriteSheetY][1]);
            return super.move(x, y, level);

        }
        if (x > 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[numberOnSpriteSheetY][4]);
            return super.move(x, y, level);

        } else if (x < 0) {
            super.getSprite().setRegion(super.getRegionOfTexture()[numberOnSpriteSheetY][10]);
            return super.move(x, y, level);

        }
        return false;
    }
}
