package com.eyesoftware.vitality;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Prepares the levels and adds them to the level list.
 */
public class LevelLoader {
    private List<Item> itemsInLevel = new ArrayList<Item>();
    private List<Entity> entitiesInLevel = new ArrayList<Entity>();
    private Level createdLevel;
    private boolean[][] collision = null;
    private TmxMapLoader loader = new TmxMapLoader();
    private TiledMap map;
    public static int levelCount = 0;
    private Entity levelExit;

    public void select(String id, Vitality game) {
        map = loader.load(id + ".tmx");
        loadEnemies();
        loadExit(game);
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("Tile Layer 2");
        collision = new boolean[layer.getWidth()][layer.getHeight()];
        createdLevel = new Level(map, id, collision.clone(), entitiesInLevel);
        LevelLoader.levelCount++;
        entitiesInLevel.removeAll(entitiesInLevel);
        for(int i = 0; i<collision.length;i++){
            for(int j = 0; j<collision[i].length;j++){
                collision[i][j]=false;
            }
        }


    }
    private void loadExit(Vitality game){
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(3);
        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                try {
                    TiledMapTile tile = cell.getTile();
                    String levelId = tile.getProperties().getKeys().next();
                    if(tile.getProperties().containsKey("exit")){
                        levelExit = new LevelExit(x * Tile.TILE_SIZE, y * Tile.TILE_SIZE, new Sprite((new Texture(Gdx.files.internal("tiles.png"))),Tile.TILE_SIZE,Tile.TILE_SIZE),game,levelId);
                        entitiesInLevel.add(levelExit);
                        Gdx.app.log("Info", "Exit at: " + "(" + x + "," + y + ")");
                    }
                } catch (NullPointerException e) {
                    continue;
                }
            }

        }

    }
    private List<Entity> loadEnemies() {
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(2);

        for (int x = 0; x < layer.getWidth(); x++) {
            for (int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                try {
                    TiledMapTile tile = cell.getTile();
                    if (tile != null) {

                        entitiesInLevel.add(new Enemy(tile.getTextureRegion().getRegionY() / Tile.TILE_SIZE, 1, x * Tile.TILE_SIZE, y * Tile.TILE_SIZE, new Sprite((new Texture(Gdx.files.internal("monsters.png"))), Tile.TILE_SIZE, Tile.TILE_SIZE)));
                        Gdx.app.log("Info", "Enemy at: " + "(" + x + "," + y + ")");
                    }
                } catch (NullPointerException e) {

                    continue;
                }
            }

        }
        layer.setVisible(false);
        return entitiesInLevel;
    }

    public List<Entity> getentitiesInLevel() {
        return entitiesInLevel;
    }

    public List<Item> getitemsInLevel() {
        return itemsInLevel;
    }

    public Level getLevel() {
        return createdLevel;
    }
}
