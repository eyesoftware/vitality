package com.eyesoftware.vitality;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import java.util.Random;

/**
 * The Abstract class for implementation of entities. All players and NPCs are entities.
 *
 * @Author Bobby Ross
 */
public abstract class Entity {
    private TextureRegion[][] regionOfTexture;
    private Sprite sprite;
    private int x;
    private int y;
    private int id;

    public static boolean returnCollision(Entity e1, Entity e2){
        if(e1.getX() == e2.getX() && e1.getY() == e2.getY() && !e1.equals(e2)){
            System.out.println("Collision Detected: Entity 1: " + e1 + " Entity 2: " + e2);
            return true;
        }
        return false;
    }
    /**
     * Checks collision, and executes collided with
     * @param e1
     * @param e2
     */
    public static void checkCollision(Entity e1, Entity e2) {
        /* TODO: checks collision, if they collide then it calls the collides with method */
        if(e1.getX() == e2.getX() && e1.getY() == e2.getY() && !e1.equals(e2)){
            e1.Collideswith(e2);
            e2.Collideswith(e1);
        }
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    public void setRegionOfTexture(TextureRegion[][] regionOfTexture) {
        this.regionOfTexture = regionOfTexture;
    }


    public TextureRegion[][] getRegionOfTexture() {
        return regionOfTexture;
    }

    public abstract void Collideswith(Entity e);

    /**
     * Calculate a move to a given entity
     * @param e
     */
    public  void calculateMove(Entity e, Level level, int delta){
        //Random random = new Random();
       // moveTo((random.nextInt(10) -5) * Tile.TILE_SIZE + e.getX(), (random.nextInt(10) -5) * Tile.TILE_SIZE + e.getY(), level);
        if(e.getX() > this.x && e.getY() == this.y){
            if(!move(Tile.TILE_SIZE,0,level)){
                //return;
            }
        }
        else if(e.getX() < this.x && e.getY() == this.y){
            if(!move(-Tile.TILE_SIZE,0,level)){
                //return;
            }
        }
        if(e.getX() ==this.x && e.getY() > this.y){
            if(!move(0,Tile.TILE_SIZE,level)){
                //return;
            }
        }
        else if(e.getX() == this.x && e.getY() < this.y){
            if(!move(0,-Tile.TILE_SIZE,level)){
                //return;
            }
        }

    }

    public boolean move(int x, int y, Level level) {

        if (level.checkCollision((this.x + x) / Tile.TILE_SIZE, this.y / Tile.TILE_SIZE)) {
            return false;
        } else if (level.checkCollision(this.x / Tile.TILE_SIZE, (this.y + y) / Tile.TILE_SIZE)) {
            return false;
        }
        if(this.getId() != 0) {
            for (Entity e : level.getEntities()) {
                if (Entity.returnCollision(this, e)) {
                    return false;
                }
            }
        }
        this.x += x;
        this.y += y;
        for(Entity e: level.getEntities()) {
            Entity.checkCollision(this, e);
        }


        return true;
    }

    /**
     * Move the entity to
     *
     * @param x        X-Coordinate
     * @param y        Y-COordinate
     * @param level    Level to traverse
     */
    public void moveTo(int x, int y, Level level, int delta) {
        if (this.x / Tile.TILE_SIZE < x / Tile.TILE_SIZE) {

            if (this.move(Tile.TILE_SIZE, 0, level)) {

                Gdx.graphics.requestRendering();
                moveTo(x, y, level, delta);
            }
        }
        if (this.x / Tile.TILE_SIZE > x / Tile.TILE_SIZE) {
            if (this.move(-Tile.TILE_SIZE, 0, level)) {

                Gdx.graphics.requestRendering();
                moveTo(x, y, level,delta);
            }
        }
        if (this.y / Tile.TILE_SIZE < y / Tile.TILE_SIZE) {
            if (this.move(0, Tile.TILE_SIZE, level)) {

                Gdx.graphics.requestRendering();
                moveTo(x, y, level,delta);
            }
        }
        if (this.y / Tile.TILE_SIZE > y / Tile.TILE_SIZE) {
            if (this.move(0, -Tile.TILE_SIZE, level)) {

                Gdx.graphics.requestRendering();
                moveTo(x, y, level,delta);
            }
        }
        Gdx.graphics.requestRendering();


    }

    public Entity(int id, int x, int y, Sprite sprite) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        regionOfTexture = new TextureRegion(sprite.getTexture()).split(Tile.TILE_SIZE,Tile.TILE_SIZE);
    }
    public int getId(){
        return id;
    }
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void render(SpriteBatch batch) {

        sprite.draw(batch);
        sprite.setPosition(x, y);


    }


}
