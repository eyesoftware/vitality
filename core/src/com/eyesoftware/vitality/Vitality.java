package com.eyesoftware.vitality;

import box2dLight.ConeLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.ArrayList;
import java.util.List;

public class Vitality extends ApplicationAdapter implements InputProcessor {
    private static LevelLoader loader = new LevelLoader();
    SpriteBatch batch;
    Level currentLevel;
    List<Level> levels = new ArrayList<Level>();
    Player player;
    Texture img;
    TiledMap tiledMap;
    OrthographicCamera camera;
    TiledMapRenderer tiledMapRenderer;
    SpriteBatch sb;
    BitmapFont textOutput;
    int createTimes = 0;
    RayHandler rayHandler;
    World world;

    @Override
    public void create() {
        Gdx.app.log("Directory",
                System.getProperty("user.dir"));
        Gdx.graphics.setVSync(true);
        world = new World(new Vector2(0,-9.8f),false);
        textOutput = new BitmapFont();
        if(createTimes == 0) {
            switchLevel("0");
            createTimes++;
        }
        try {
            player = Player.constructPlayer(currentLevel.getPlayerSpawn(),this);
        } catch (GdxRuntimeException e) {
            /*Skin skin = new Skin(Gdx.files.internal(Assets.uiskin));
			Dialog errWindow = new Dialog("Error", skin);
			TextArea text = new TextArea(e.getMessage(), skin);
			errWindow.add(text); */
            Gdx.app.error("Exception", e.toString());
            Gdx.app.error("Exception Message", e.getMessage());
            Gdx.app.error("Informal Error", "player.png not found in current ");
            Gdx.app.error("Directory", "Working Directory = " +
                    System.getProperty("user.dir"));
            Gdx.app.log("Formal Error Report", "An Error has Occured: 1");
            Gdx.app.log("Exit Message", "This application cannot continue");
            Gdx.app.exit();
        }
        catch (PlayerSpawnNotFound e){
            Gdx.app.error("Exception",e.toString());
            Gdx.app.error("Exception Message", e.getMessage());
            Gdx.app.error("Informal Error","A Spawn Point has not been put onto this map. Please refer to the map documentation");
            Gdx.app.log("Formal Error Report", "AN error has occured: -1");
            Gdx.app.log("Exit Message", "This application cannot continue");
            Gdx.app.exit();
        }
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, w, h);
        camera.update();

        tiledMapRenderer = new OrthogonalTiledMapRenderer(currentLevel.getLevelMap());

        Gdx.input.setInputProcessor(this);

    }

    /**
     * Clears the current level, list of entities and items, and calls a new level.
     *
     * @param levelId
     */
    public void switchLevel(String levelId) {
        loader.select(levelId,this);
        levels.add(loader.getLevel());
        currentLevel = loader.getLevel();

    }

    @Override
    public void render() {

        Gdx.gl20.glViewport( 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() );
        Gdx.gl20.glClearColor( 0, 0, 0, 1 );
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        camera.update();

        camera.position.set(player.getX(), player.getY(), 0);
        tiledMapRenderer.setView(camera);

        rayHandler = new RayHandler(world);
        rayHandler.setCombinedMatrix(camera.combined);
        batch.setProjectionMatrix(camera.combined);
        tiledMapRenderer.render();


        batch.begin();
        textOutput.setColor(Color.RED);

        Gdx.graphics.setTitle("Vitality [DEBUG] FPS: " + Gdx.graphics.getFramesPerSecond());

        player.render(batch);

        currentLevel.render(batch);
        batch.end();

    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.LEFT)
            player.move(-Tile.TILE_SIZE, 0, currentLevel);

        if (keycode == Input.Keys.RIGHT)
            player.move(Tile.TILE_SIZE, 0, currentLevel);

        if (keycode == Input.Keys.UP)
            player.move(0, Tile.TILE_SIZE, currentLevel);

        if (keycode == Input.Keys.DOWN)
            player.move(0, -Tile.TILE_SIZE, currentLevel);

        if (keycode == Input.Keys.A)
            player.move(-Tile.TILE_SIZE, 0, currentLevel);

        if (keycode == Input.Keys.D)
            player.move(Tile.TILE_SIZE, 0, currentLevel);

        if (keycode == Input.Keys.W)
            player.move(0, Tile.TILE_SIZE, currentLevel);

        if (keycode == Input.Keys.S)
            player.move(0, -Tile.TILE_SIZE, currentLevel);

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        return false;
    }

    @Override
    public boolean keyTyped(char character) {

        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (button == Input.Buttons.LEFT) {
            Vector3 screenVec = new Vector3();
            screenVec.set(screenX, screenY, 0);
            Vector3 finalVec = camera.unproject(screenVec);
            screenX = (int) finalVec.x;
            screenY = (int) finalVec.y;
            player.moveTo(screenX, screenY, currentLevel, (screenX + screenX) / 2);


        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
