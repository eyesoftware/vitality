package com.eyesoftware.vitality;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Bobby on 12/11/2014.
 */
public abstract class Item {
    private int x;
    private int y;
    private Sprite sprite;

    public abstract void applyItem();

    public Item(int x, int y, Sprite sprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
    }

    public void render(SpriteBatch batch) {

        sprite.draw(batch);

    }
}
